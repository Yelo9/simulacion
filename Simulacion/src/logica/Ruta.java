/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;
import java.util.ArrayList;
import logica.Coordenada;

/**
 *
 * @author Estudiante
 */
public class Ruta {
    
    private int idRuta;
    private int cantidadPC;
    private String destino;
    private Coordenada[] coordenadas;
    //Variables coordenadas
    int xPEntrada = 36;
    int yPEntrada = 64;
    int xPIEA = 30;//Inicio de escaleras auditorio
    int yPIEA = 57;//Inicio de escaleras auditorio
    int xFI = 38;//Frente Investigaciones x
    int yFI = 59;//Frente Investigaciones y
    int xES = 55;//Esquina Ingeneirìa de sistemas x
    int yES = 68;//Esquina Ingeneirìa de sistemas y
    int xSD = 60;//Sillas Derecho  x
    int ySD = 75;//Sillas Derecho y
    int xORI = 60;//ORIIx
    int yORI = 79;//ORIIy
    int xS4 = 40; //Sistemas 4
    int yS4 = 68; //Sistemas 4
    int xTic =60;
    int yTic =61;
    int xEEC = 24; //escaEntradaCafeteria
    int yEEC = 50; //escaEntradaCafeteria
    int xDG = 27; //Descenso GYM
    int yDG = 33; //Descenso GYM
    int xEC = 22; //Entrada camerinos
    int yEC = 33; //Entrada camerinos 
    int xBP2 = 18;//Edificio  BSegundo piso
    int yBP2 = 15;//Edificio  BSegundo piso
    int xBP3 = 18;//Edificio  BTercer piso
    int yBP3 = 15;//Edificio  BTercer piso
    int xIN23 = 23;// Ruta sala de estar
    int yIN23 = 34;
    int xIN34 = 23;
    int yIN34 = 34;
    int xESE = 23;// Entrada sala de estar
    int yESE = 34;
    int xPV = 60;// Puerta Vicerectoria 
    int yPV = 55;   
    int xS3 = 60;
    int ys3 = 55;
    int xSP = 34;
    int ySP = 55;
    int xEB = 54;
    int yEB = 34;
    int xIEG = 22;
    int yIEG = 50;
    int xFEG = 21;
    int yFEG = 49;
    int xS103 =24;
    int yS103 = 55;
    
 public Ruta(){
     
 }
    public Ruta(int idRuta, String destino, Coordenada[] coordenadas) {
        this.idRuta = idRuta;
        this.destino = destino;
        this.coordenadas = coordenadas;
        
    }
    
 public ArrayList rutaOrii(){
     idRuta=04;
     destino="Orii";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> orii= new ArrayList<int[]>();
     orii.add(coord.entrada());
     orii.add(coord.orii());
     cantidadPC=orii.size();
     return orii;
 }
 
 public ArrayList facultadSistemas(){
     idRuta=001;
     destino="Facultad Sistemas";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.orii());
     c.add(coord.sillasDerecho());
     c.add(coord.esquinaIngSistemas());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList facultadDerecho(){
     idRuta=002;
     destino="Facultad Derecho";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.orii());
     c.add(coord.sillasDerecho());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList adminC(){
     idRuta=003;
     destino="Administrativo C";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.orii());
     c.add(coord.sillasDerecho());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList salasSistemas(){
     idRuta=005;
     destino="Salas Sistemas";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.orii());
     c.add(coord.sillasDerecho());
     c.add(coord.tic());
     c.add(coord.sistemas4());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList salonesC(){
     idRuta=006;
     destino="Salones C";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.orii());
     c.add(coord.sillasDerecho());
     c.add(coord.tic());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList biblioteca(){
     idRuta=007;
     destino="Biblioteca";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.entradaCamerinos());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList cafeteria(){
     idRuta=8;
     destino="Cafeteria";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.escaEntradaCafeteria());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList auditorios(){
     idRuta=9;
     destino="Auditorios";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList laboratorios(){
     idRuta=10;
     destino="Laboratorios Fisica";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     c.add(coord.inter23());
     cantidadPC=c.size();
     return c;
 }
 
 public ArrayList laboratoriosE(){
     idRuta=11;
     destino="Laboratorios electronica";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     c.add(coord.inter23());
     c.add(coord.inter34());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList gym(){
     idRuta=12;
     destino="Gimnasio y camerinos";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.escaEntradaCafeteria());
     c.add(coord.entradaCamerinos());
     c.add(coord.descensoGym());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList edificioB(){
     idRuta=13;
     destino="Edificio B 1- 2";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.escaEntradaCafeteria());
     c.add(coord.pasilloBsegPiso());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList edificioB3(){
     idRuta=14;
     destino="Edificio B 3-4";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.escaEntradaCafeteria());
     c.add(coord.pasilloBsegPiso());
     c.add(coord.pasilloBterPiso());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList salaE(){
     idRuta=15;
     destino="Sala Estar";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.escaEntradaCafeteria());
     c.add(coord.inter23());
     c.add(coord.inter34());
     c.add(coord.entradaSalaStar());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList adminA(){
     idRuta=16;
     destino="Administrativa A";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     c.add(coord.frenteInvestigaciones());
     c.add(coord.puertaVicerrectoria());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList salaP(){
     idRuta=17;
     destino="Sala de Profesores";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     c.add(coord.frenteInvestigaciones());
     c.add(coord.inter23());
     c.add(coord.sistemas3());
     c.add(coord.salaProfesores());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList enfermeria(){
     idRuta=18;
     destino="Enfermeria";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.estatuaBronce());
     
     cantidadPC=c.size();
     return c;
 }
 public ArrayList salonesA(){
     idRuta=19;
     destino="Salones A";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     c.add(coord.frenteInvestigaciones());
     c.add(coord.inter23());
     cantidadPC=c.size();
     return c;
 }
 public ArrayList comunicaciones(){
     idRuta=20;
     destino="Administrativa A";
     Coordenada coord = new Coordenada();
     
     ArrayList<int[]> c= new ArrayList<int[]>();
     c.add(coord.entrada());
     c.add(coord.inicioEscalerasAuditorio());
     c.add(coord.frenteInvestigaciones());
     c.add(coord.puertaVicerrectoria());
     c.add(coord.salaProfesores());
             
     cantidadPC=c.size();
     return c;
 }
    public int getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(int idRuta) {
        this.idRuta = idRuta;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Coordenada[] getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenada[] coordenadas) {
        this.coordenadas = coordenadas;
    }
    
}

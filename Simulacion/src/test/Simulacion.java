/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import static logica.Coordenada.entrada;
import static logica.Coordenada.inicioEscalerasAuditorio;

/**
 *
 * @author Estudiante
 */
import static logica.Coordenada.entrada;
import static logica.Coordenada.inicioEscalerasAuditorio;
import logica.Ruta;

public class Simulacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int entrada[] = entrada();
        System.out.println("X: " +entrada[0]+ " & Y: " + entrada[1]);
        int inicioEscalerasAuditorio[] = inicioEscalerasAuditorio();
        System.out.println("X: " +inicioEscalerasAuditorio[0]+ " & Y: " + inicioEscalerasAuditorio[1]);
        
        // Este es un código para probar la ruta, trae los valores de arrayList que genera el metodo de cada ruta con sus respectivas posiciones
        Ruta r = new Ruta();
        int[] aux = new int[r.salaP().size()];
        for (int i=0;i<r.salaP().size();i++){
            aux=(int[]) r.salaP().get(i);
            System.out.println("x: "+aux[0] + " y: "+aux[1] );
        }
        
    } 
    
     
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Estudiante
 */
public class Persona {
 
    private int idPersona;
    private int xPer;
    private int yPer;
    private Ruta ruta;
    private boolean genero;
    private byte piso;

    public Persona(int idPersona, int xPer, int yPer, Ruta ruta, boolean genero, byte piso) {
        this.idPersona = idPersona;
        this.xPer = xPer;
        this.yPer = yPer;
        this.ruta = ruta;
        this.genero = genero;
        this.piso = piso;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getxPer() {
        return xPer;
    }

    public void setxPer(int xPer) {
        this.xPer = xPer;
    }

    public int getyPer() {
        return yPer;
    }

    public void setyPer(int yPer) {
        this.yPer = yPer;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public boolean isGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }

    public byte getPiso() {
        return piso;
    }

    public void setPiso(byte piso) {
        this.piso = piso;
    }
    
    
}
